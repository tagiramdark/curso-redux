import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { NativeScriptModule, NativeScriptFormsModule } from "@nativescript/angular";
import {ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TodoComponent } from "./todo/todo.component";
import { AddComponent } from "./todo/add/add.component";
import { ItemComponent } from "./todo/item/item.component"
import { TaskListComponent } from "./todo/tasklist/tasklist.component";
@NgModule({
  bootstrap: [AppComponent],
  imports: [NativeScriptModule, AppRoutingModule,ReactiveFormsModule, NativeScriptFormsModule],
  declarations: [AppComponent, TodoComponent, AddComponent, ItemComponent, TaskListComponent],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule { }
