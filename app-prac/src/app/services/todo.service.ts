import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

export class Task {
  public id: number = 0;
  public texto: string = "";
  public completado: boolean = false;

  constructor(texto: string) {
    this.texto = texto;
    this.id = Math.random();
  }
}

@Injectable({
  providedIn: "root",
})
export class TaskService {
  private tareasSubject = new BehaviorSubject<Task[]>([
    new Task("Tarea 1"),
    new Task("Tarea 2"),
    new Task("Tarea 3"),
    new Task("Tarea 4"),
  ]);
  tareas$=this.tareasSubject.asObservable();
  get Tasks(): BehaviorSubject<Task[]> {
    return this.tareasSubject;
  }

  getTask(id: number): Task {
    return this.tareasSubject.value.filter((item) => item.id === id)[0];
  }
  addTask(task: Task) {
    const actualTasks = this.tareasSubject.value;
    const updateTasks = [...actualTasks, task];
    this.tareasSubject.next(updateTasks);
  }
  deleteTask(id: number) {
    this.tareasSubject.next(this.tareasSubject.value.filter((x) => x.id !== id));
  }
  editTask(id: number, texto: string) {
    this.tareasSubject.next(
      this.tareasSubject.value.map((task) => {
        if (id === task.id) {
          return { ...task,texto: texto };
        } else {
          return task;
        }
      })
    );
  }
  toogleTask(id:number){
    this.tareasSubject.next(
        this.tareasSubject.value.map((task) => {
          if (id === task.id) {
            return { ...task,completado:!task.completado };
          } else {
            return task;
          }
        })
      );
  }
}
