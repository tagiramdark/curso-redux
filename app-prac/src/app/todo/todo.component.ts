import { Component, OnInit } from "@angular/core";
import {Page} from '@nativescript/core'
//import { Store } from '@ngrx/store';
//import { AppState } from 'src/app/app.reducer';
//import * as actions from '../todo.actions';
@Component({
  selector: "ns-todo",
  templateUrl: "./todo.component.html",
})
export class TodoComponent implements OnInit {
  completado: boolean = false;
  constructor(page:Page) {
    page.actionBarHidden=true;
  }

  ngOnInit(): void {
   
  }
  toogleAll() {
    this.completado = !this.completado;
    // this.store.dispatch(actions.toogleAll({check:this.completado}));
  }
}
