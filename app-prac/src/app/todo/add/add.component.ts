import { Component, NgModule, OnInit } from '@angular/core';
import { Task, TaskService } from "../../services/todo.service";
import { FormControl,Validators } from '@angular/forms';
@Component({
  selector: 'add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
})

export class AddComponent implements OnInit {

  task: FormControl=new FormControl('',Validators.required);
  constructor(private taskService: TaskService) { }

  ngOnInit(): void { }
  agregar() {
    if (this.task.invalid) return;
    this.taskService.addTask(new Task(this.task.value))
    this.task.reset();
  }
}
