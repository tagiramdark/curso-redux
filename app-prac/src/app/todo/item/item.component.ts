import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { Task, TaskService } from "../../services/todo.service";

@Component({
  selector: "item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.css"],
})
export class ItemComponent implements OnInit {
  @Input() task: Task;

  txtInput!: FormControl;
  editando: boolean = false;

  @ViewChild("inputFisico") txtInputFisico!: ElementRef;

  constructor(private tasksService: TaskService) {}

  ngOnInit(): void {
    this.txtInput = new FormControl(this.task.texto, Validators.required);
  }

  toogle() {
    this.tasksService.toogleTask(this.task.id);
  }
  editar() {
    this.editando = true;
    this.txtInput.setValue(this.task.texto);
  }
  terminarEdicion() {
    this.editando = false;
    if (this.txtInput.invalid || this.txtInput.value === this.task.texto)
      return;
    this.tasksService.editTask(this.task.id, this.txtInput.value);
  }
  borrar() {
    this.tasksService.deleteTask(this.task.id);
  }
}
