import { Component, OnInit } from "@angular/core";
//import { FormControl, Validators } from '@angular/forms';

import { Task, TaskService } from "../../services/todo.service";

@Component({
  selector: "tasklist",
  templateUrl: "./tasklist.component.html",
  styleUrls: ['./tasklist.component.css'],
})
export class TaskListComponent implements OnInit {  
  tasks: Task[] = [];
  constructor(private tasksService: TaskService) {
    this.tasksService.tareas$.subscribe((x) => {
      this.tasks = x;
    });
  }
  ngOnInit() { }
}
