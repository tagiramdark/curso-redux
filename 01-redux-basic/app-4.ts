import { contadorReducer } from './contador/contador.reducer';
import {createStore, Store} from 'redux'
import { incrementadorAction } from './contador/contador.actions';
const store: Store=createStore(contadorReducer);

store.subscribe(()=>{
    console.log('Subs: ',store.getState());
})

store.dispatch(incrementadorAction);

