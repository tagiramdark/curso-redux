import { Action, createReducer, on } from '@ngrx/store';
import { Todo } from './models/todo.model';
import * as actions from './todo.actions';

export const estadoInicial: Todo[] = [
  new Todo('Salvar al Mundo'),
  new Todo('Jugar Videojuegos'),
  new Todo('Comer Sabritas'),
];

const _todoReducer = createReducer(
  estadoInicial,
  on(actions.crear, (state, { texto }) => [...state, new Todo(texto)]),
  on(actions.toogle, (state, { id }) => {
    return state.map((todo) => {
      if (id === todo.id) {
        return {
          ...todo,
          completado: !todo.completado,
        };
      } else {
        return todo;
      }
    });
  }),
  on(actions.editar, (state, { id, texto }) => {
    return state.map((todo) => {
      if (id === todo.id) {
        return {
          ...todo,
          texto: texto,
        };
      } else {
        return todo;
      }
    });
  }),
  on(actions.borrar, (state, { id }) => state.filter((todo) => todo.id !== id)),
  on(actions.toogleAll,(state, { check }) => state.map(todo=>{return {...todo,completado: check}}))
);

export function todoReducer(state: any, action: Action) {
  return _todoReducer(state, action);
}
