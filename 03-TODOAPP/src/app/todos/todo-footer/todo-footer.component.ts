import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as actions from 'src/app/filtro/filtro.actions';
@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.css'],
})
export class TodoFooterComponent implements OnInit {
  filtroActual: actions.filtrosValidos = actions.filtrosValidos.todos;
  filtros: actions.filtrosValidos[] = [
    actions.filtrosValidos.todos,
    actions.filtrosValidos.pendientes,
    actions.filtrosValidos.completados,
  ];
  pedientes: number = 0;
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.subscribe((state) => {
      this.filtroActual = state.filtro;
      this.pedientes=state.todos.filter(x=>!x.completado).length
    });
  }
  getNameFiltro(value: actions.filtrosValidos) {
    return actions.filtrosValidos[value];
  }
  cambiarFiltro(filtro: actions.filtrosValidos) {
    this.store.dispatch(actions.setFiltro({ filtro }));
  }
}
