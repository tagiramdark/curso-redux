import { createAction, props } from '@ngrx/store';

export enum filtrosValidos {
  "todos"=0,
  "pendientes"=1,
  "completados"=2,
}

export const setFiltro = createAction(
  '[Filtro] Set Filtro',
  props<{ filtro: filtrosValidos }>()
);
